// @flow
import React from 'react';
import Chart from 'react-apexcharts';
import { Card, CardBody } from 'reactstrap';

// simple line chart
const getData = (data) =>{
    let values = [];
    if(data.data)
    {
        let obj1 = {name: "High", data:[]}
        let obj2 = {name: "Low", data:[]}
        for(var i = 0; i < data.data.length; i++)
        {
            if(data.data[i].quality === "High")
            {
                let value = data.data[i].count
                obj1.data.push(value)
            }
            if(data.data[i].quality === "Low")
            {
                let value = data.data[i].count
                obj2.data.push(value)
            }
        }
        if(obj1.data.length > 0)
        {
            values.push(obj1)
        }
        if(obj2.data.length > 0)
        {
            values.push(obj2)
        }
    }

    return values
}


function LineChart(props) {
    const apexLineChartWithLables = {
        chart: {
            height: 380,
            type: 'line',
            zoom: {
                enabled: false,
            },
            toolbar: {
                show: false,
            },
        },
        colors: ['#727cf5', '#0acf97'],
        dataLabels: {
            enabled: true,
        },
        stroke: {
            width: [3, 3],
            curve: 'smooth',
        },
        title: {
            text: 'Average High & Low Temperature',
            align: 'left',
            style: {
                fontSize: '14px',
            },
        },
        grid: {
            row: {
                colors: ['transparent', 'transparent'], // takes an array which will be repeated on columns
                opacity: 0.2,
            },
            borderColor: '#f1f3fa',
        },
        markers: {
            style: 'inverted',
            size: 6,
        },
        xaxis: {
            categories: ['2001', '2002', '2003', '2004', '2005', '2006', '2007'],
            title: {
                text: 'Year',
            },
        },
        yaxis: {
            title: {
                text: 'Temperature',
            },
            min: 5,
            max: 100,
        },
        legend: {
            position: 'top',
            horizontalAlign: 'right',
            floating: true,
            offsetY: -25,
            offsetX: -5,
        },
        responsive: [
            {
                breakpoint: 600,
                options: {
                    chart: {
                        toolbar: {
                            show: false,
                        },
                    },
                    legend: {
                        show: false,
                    },
                },
            },
        ],
    };

    const apexLineChartWithLablesData = [
        {
            name: 'High - 2018',
            data: [28, 29, 33, 36, 32, 32, 33],
        },
        {
            name: 'Low - 2018',
            data: [12, 11, 14, 18, 17, 13, 13],
        },
    ];

    return (
        <Card>
            <CardBody>
                <h4 className="header-title mb-3">Line with Data Labels</h4>
                <Chart
                    options={apexLineChartWithLables}
                    series={getData(props.data)}
                    type="line"
                    className="apex-charts"
                />
            </CardBody>
        </Card>
    );
};

export default LineChart;
