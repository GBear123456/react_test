// @flow
import React from 'react';
import Chart from 'react-apexcharts';
import { Card, CardBody } from 'reactstrap';

// simple bar chart
const getData = (data) =>{
    let values = [];
    if(data.data)
    {
        let obj1 = {name: "High", data:[]}
        let obj2 = {name: "Low", data:[]}
        for(var i = 0; i < data.data.length; i++)
        {
            if(data.data[i].quality === "High")
            {
                let value = data.data[i].count
                obj1.data.push(value)
            }
            if(data.data[i].quality === "Low")
            {
                let value = data.data[i].count
                obj2.data.push(value)
            }

        }
        if(obj1.data.length > 0)
        {
            values.push(obj1)
        }
        if(obj2.data.length > 0)
        {
            values.push(obj2)
        }
    }
    return values
}


function BarChart(props) {
    const apexBarChartOpts = {
        chart: {
            height: 380,
            type: 'bar',
            toolbar: {
                show: false,
            },
        },
        plotOptions: {
            bar: {
                horizontal: false,
                dataLabels: {
                    position: 'top',
                },
            },
        },
        dataLabels: {
            enabled: true,
            offsetX: -6,
            style: {
                fontSize: '12px',
                colors: ['#fff'],
            },
        },
        colors: ['#fa5c7c', '#6c757d'],
        stroke: {
            show: true,
            width: 1,
            colors: ['#fff'],
        },

        xaxis: {
            categories: [2001, 2002, 2003, 2004, 2005, 2006, 2007],
        },
        legend: {
            offsetY: -10,
        },
        states: {
            hover: {
                filter: 'none',
            },
        },
        grid: {
            borderColor: '#f1f3fa',
        },
    };

    const apexBarChartData = [
        {
            name: 'Series 1',
            data: [44, 55, 41, 64, 22, 43, 21],
        },
        {
            name: 'Series 2',
            data: [53, 32, 33, 52, 13, 44, 32],
        },
    ];

    

    return (
        <Card>
            <CardBody>
                <h4 className="header-title mb-3">Bar Chart</h4>
                <Chart options={apexBarChartOpts} series={getData(props.data)} type="bar" className="apex-charts" />
            </CardBody>
        </Card>
    );
};

export default BarChart;
