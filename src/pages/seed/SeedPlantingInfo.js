import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Row, Col, Card, CardBody} from 'reactstrap';
import LangUtils from '../../helpers/langUtils';
import { ProductTypes, SelectAll, State, Units } from '../../helpers/constant';
import {getSeed} from '../../redux/actions';
import LoaderWidget from '../../components/Loader';

class SeedPlantingInformation extends Component {
    _isMounted = false;

    constructor(props, context) {
        super(props, context);
        this.state = {
            selected_production :{},
            selected_seed_info: {},
            selected_seed_infos: [],              
            name :'' ,            
            seed_infos: []                                  
        }
    }

    componentWillMount() {
        this.props.getSeed('')   
    }

    componentDidMount() {
        this._isMounted = true;
             
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    NumberColumn = (cell, row, rowIndex, extraData) => {
        return (
            <React.Fragment>
                <span>
                    <a href="#"><u style={{color:'#089e74'}}>{row.field_year}</u></a>
                </span>
            </React.Fragment>
        );
    };

    

    ClassColumn = (cell, row, rowIndex, extraData) => {
        const{user}= this.props
        return (
            <React.Fragment>                
                <span>{LangUtils.getLanguage(user.lang, ProductTypes[row.production_type].label)}</span>                
            </React.Fragment>
        );
    }

    UnitColumn = (cell, row, rowIndex, extraData) => {        
        const{user}= this.props
        return (
            <React.Fragment>                
                <span>{LangUtils.getLanguage(user.lang, Units[row.unit].label)}</span>                
            </React.Fragment>
        );
    }

    getOptions = (type) => {
        const{user}= this.props
        var options = []
        options.push({
            value:SelectAll,
            label:LangUtils.getLanguage(user.lang, "select.SelectAll")
        })
        if (type === 'production_type') {
            ProductTypes.map(p_t => {
                options.push({
                    value:p_t.value,
                    label:LangUtils.getLanguage(user.lang, p_t.label)
                })
            })
        } else if (type === "Status") {
            State.map(state => {
                options.push({
                    value:state.value,
                    label:LangUtils.getLanguage(user.lang, state.label)
                })
            })
        }
        return options;
    }

    columns = () => {
        const {user} = this.props
        return [        
            {
                dataField: 'field_year',
                text: "Year",
                sort: false,
                formatter: this.NumberColumn
            },
            {
                dataField: 'field_fulfilled',
                text: "Fulfilled",
                sort: false,
            },
            {
                dataField: 'field_unfulfilled',
                text: "Unfulfilled",
                sort: false,
            }       
        ];
    } 
    
    render() {
        const {user, loading, seed_infos} = this.props  
        const { name } = this.state
        return (
            <React.Fragment>
                <Row>
                    <Col>
                        <Card>
                            <CardBody>
                            {loading && <LoaderWidget />}
                                <div className="table-responsive">
                                    <table className="table table-bordered table-hover table-striped">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Year</th>
                                                <th>Quality</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {
                                                seed_infos.data && seed_infos.data.map((data, index) => { 
                                                    return (
                                                        <tr key={'data-' + index}>
                                                            <td>{index + 1}</td>
                                                            <td>{data.year}</td>
                                                            <td>{data.quality}</td>
                                                            <td>{data.status === 1 ? "Fulfilled" : "Unfulfilled"}</td>
                                                        </tr>
                                                    )
                                                })
                                            }
                                        </tbody>
                                    </table>
                                </div>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
            </React.Fragment>
        );
    }   
    
}
/**
     @params@
     //This sets the stats values to props while interactions. 
*/
const mapStateToProps = state => {
    return {
        user: state.Auth.user,
        loading: state.Seed.loading,
        seed_infos: state.Seed.seed_infos,
    };
};

export default withRouter(
    connect(
        mapStateToProps,
        {getSeed}
    )(SeedPlantingInformation)
);