/** 
 * @template data flow
*/
import {
    /*SEED PLANTING INFORMATION */ 
    GET_SEED_INFORMATION, GET_SEED_INFORMATION_SUCCESS, GET_SEED_INFORMATION_FAILED,
    FILTER, FILTER_SUCCESS, FILTER_FAIL
} from './constants';

export const filter = (quality, status) => ({
    type: FILTER,
    payload: {quality: quality, status: status}
})

export const filterFailed = (error) => ({
    type: FILTER_FAIL,
    payload: {error}
})

export const filterSuccess = (data) => ({
    type: FILTER_SUCCESS,
    payload: {data}
})
 

/*SEED INFORMATION*/ 
export const getSeed = (search) => ({
    type: GET_SEED_INFORMATION,
    payload: { search },
});
export const getSeedInformationSuccess = (data) => ({
    type: GET_SEED_INFORMATION_SUCCESS,
    payload: data,
});

export const getSeedInformationFailed = (error) => ({
    type: GET_SEED_INFORMATION_FAILED,
    payload: error,
});
