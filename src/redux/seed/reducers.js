/** 
 * @template data flow
*/
import {GET_SEED_INFORMATION, 
    GET_SEED_INFORMATION_SUCCESS,
    GET_SEED_INFORMATION_FAILED,
    FILTER, FILTER_SUCCESS, FILTER_FAIL} from './constants'

import _ from 'lodash'

const INIT_STATE = {    
    loading: false,    

    selected_seed_info: [],
    seed_infos: [],
};
/*QRCode INFORMATION*/
const Seed = (state = INIT_STATE, action) => {
    switch (action.type) {
        case FILTER:
            return {...state, loading: true}
        case FILTER_SUCCESS:
            return {...state, loading: false, seed_infos: action.payload, error: null}
        case FILTER_FAIL:
            return {...state, loading: false, error: action.payload}

        /*SEED PLANTING INFORMATION*/
        case GET_SEED_INFORMATION:
            return { ...state, seed_infos:[], loading:true };
        case GET_SEED_INFORMATION_SUCCESS:
            return {...state, loading: false, seed_infos: action.payload, error: null}
        case GET_SEED_INFORMATION_FAILED:
            return {...state, loading: false, error: action.payload}
        default:
            return { ...state };
    }
};

export default Seed;
