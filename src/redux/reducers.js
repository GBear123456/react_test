// @flow

import { combineReducers } from 'redux';
import Layout from './layout/reducers';
import Auth from './auth/reducers';
import Seed from './seed/reducers';

export default combineReducers({
    Auth,
    Layout,
    Seed
});
