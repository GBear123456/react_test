import { lange_en } from './lang/lang-en';

class LangUtils {

    static getLanguage(lang, code)
    {
        var codes = null
        codes = lange_en
        
        var subCodes = code.split(".");

        for(var i = 0 ; i < subCodes.length ; i++) {
            var sc = subCodes[i]
            if (codes[sc])
                codes = codes[sc];
            else
                return code
        }

        return codes
    }

}

export default LangUtils;