// @flow
import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import SimpleBar from 'simplebar-react';

import logoSm from '../assets/images/logo_sm.png';
import logoDark from '../assets/images/logo-dark.png';
import logoDarkSm from '../assets/images/logo_sm_dark.png';
import logo from '../assets/images/logo.png';
import com_logo from '../assets/images/img/com-logo.png';
import profileImg from '../assets/images/users/avatar-1.jpg';

import LangUtils from '../helpers/langUtils';
import {
    Row,
    Col,
    Card,
    CardBody,
    Button,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    Form,
    FormGroup,
    Label,
    Input,
} from 'reactstrap';

import { filter } from '../redux/actions'
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

type LeftSidebarProps = {
    menuClickHandler?: {},
    hideLogo?: boolean,
    hideUserProfile?: boolean,
    isLight?: boolean,
    isCondensed: boolean,
};

class LeftSidebar extends Component<LeftSidebarProps> {
    menuNodeRef: any;

    constructor(props: LeftSidebarProps) {
        super(props);

        this.handleClick = this.handleClick.bind(this);
        this.handleOtherClick = this.handleOtherClick.bind(this);
        this.qualityChanged = this.qualityChanged.bind(this);
        this.statusChanged = this.statusChanged.bind(this);
    }

    state = {
        condensed: false,
        dataFilter: {
            quality: '',
            status: '',
        },
    }

    /**
     * Bind event
     */
    componentDidMount = () => {
        document.addEventListener('mousedown', this.handleOtherClick, false);
    };

    /**
     * Bind event
     */
    componentWillUnmount = () => {
        document.removeEventListener('mousedown', this.handleOtherClick, false);
    };

    /**
     * Handle the click anywhere in doc
     */
    handleOtherClick = (e: any) => {
        if (this.menuNodeRef.contains(e.target)) return;
        // else hide the menubar
        if (document.body) {
            document.body.classList.remove('sidebar-enable');
        }
    };

    /**
     * Handle click
     * @param {*} e
     * @param {*} item
     */
    /*:: handleClick: () => void */
    handleClick(e: {}) {
        console.log(e);
    }

    qualityChanged = e => {
        console.log( "** Quality **", e.target.value)
        let filter = e.target.value;
        let dataFilter = this.state.dataFilter;
        dataFilter.quality = filter;

        this.setState({ dataFilter: dataFilter})
    }

    statusChanged = e => {
        console.log( "** Status **", e.target.value)
        let filter = e.target.value;
        let dataFilter = this.state.dataFilter;
        dataFilter.status = filter;
        
        this.setState({ dataFilter: dataFilter})
    }

    // onCondensed(){
    //     this.setState({
    //         condensed: !this.state.condensed
    //     })

    //     let ele = document.getElementById('LSM');

    //     if(ele)
    //     {
    //         if(this.state.condensed)
    //         {
    //             ele.classList.add("condensed")
    //         }
    //         else
    //         {
    //             ele.classList.remove("condensed")
    //         }
    //     }
    // }
    render() {
        const isCondensed = this.props.isCondensed || false;
        const isLight = this.props.isLight || false;
        const hideLogo = this.props.hideLogo || false;
        const hideUserProfile = this.props.hideUserProfile || false;

        return (
            <React.Fragment>
                <div className="left-side-menu" id="LSM" ref={node => (this.menuNodeRef = node)}>
                    <React.Fragment>
                        <div className="logo logo-light" style={{textAlign:"end"}}>
                            <span className="logo-lg">
                                <div style={{fontSize:'20px', color:'white', paddingRight: '10px'}} 
                                    // onClick={
                                    //     this.onCondensed()
                                    // }
                                >
                                    <i className="mdi mdi-menu"></i>
                                </div>
                            </span>
                        </div>
                        {/* <button
                            className="button-menu-mobile open-left disable-btn"
                            onClick={this.props.openLeftMenuCallBack}>
                        </button> */}
                    </React.Fragment>

                    <Row className="mt-2 flex">
                        <Col md={12} sm={12}>
                            <Form className="mb-2">
                                <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                                    <Label htmlFor="status" className="mr-sm-2">
                                        Quality :
                                    </Label>
                                    <select type="select" name="select" id="quality" className="custom-select" onChange={(e) => this.qualityChanged(e)}>
                                        <option value="">Choose...</option>
                                        <option value="High">High</option>
                                        <option value="Low">Low</option>
                                    </select>
                                </FormGroup>
                                <FormGroup className="mb-2 mr-sm-2 mb-sm-0 mt-2">
                                    <Label htmlFor="status" className="mr-sm-2">
                                        Status:
                                    </Label>
                                    <select type="select" name="select" id="status" className="custom-select"  onChange={(e) => this.statusChanged(e)}>
                                        <option value="">Choose...</option>
                                        <option value="1">Fulfilled</option>
                                        <option value="2">Unfulfilled</option>
                                    </select>
                                </FormGroup>
                            </Form>
                        </Col>
                    </Row>
                    <Row className="mt-2">
                        <Col md={12} sm={12}>
                            <div className="mb-2 mr-sm-2 mb-sm-0 mt-2">
                                <Button outline color={'primary'} style={{marginLeft:"100px", width:"100px"}}
                                onClick={ (e) => {
                                    this.props.filter(this.state.dataFilter.quality, this.state.dataFilter.status);}}
                                >
                                    {'Apply'}
                                </Button>
                            </div>
                        </Col>
                    </Row>
                </div>
            </React.Fragment>
        );
    }
}

//export default LeftSidebar;
const mapStateToProps = state => {
    return {
        user: state.Auth.user,
        // production: state.Plant.production,
        // loading: state.Seed.loading,
        // seed_infos: state.Seed.seed_infos,
        // selected_seed_info: state.Seed.selected_seed_info,
        // seed_processing_infos: state.Seed.seed_processing_infos,
        // selected_seed_processing_info: state.Seed.selected_seed_processing_info          
    };
};

export default withRouter(
    connect(
        mapStateToProps,
        { filter }
    )(LeftSidebar)
);